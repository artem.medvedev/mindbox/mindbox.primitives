/*CREATE DATABASE mindbox OWNER postgres TABLESPACE pg_default;*/

-- Table: public.articles

-- DROP TABLE public.articles;

CREATE TABLE public.articles
(
    id bigint NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT articles_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.articles
    OWNER to postgres;

-- Table: public.tags

-- DROP TABLE public.tags;

CREATE TABLE public.tags
(
    id bigint NOT NULL,
    value text COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT tags_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.tags
    OWNER to postgres;

-- Table: public.article_tag_references

-- DROP TABLE public.article_tag_references;

CREATE TABLE public.article_tag_references
(
    id bigint NOT NULL,
    article_id bigint NOT NULL,
    tag_id bigint NOT NULL,
    CONSTRAINT article_tag_references_pkey PRIMARY KEY (id),
    CONSTRAINT article_id_fk FOREIGN KEY (article_id)
        REFERENCES public.articles (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT tag_id_fk FOREIGN KEY (tag_id)
        REFERENCES public.tags (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.article_tag_references
    OWNER to postgres;

INSERT INTO public.tags(
  id, value)
  VALUES (1, '#math'), (2, '#physics'), (3, '#biology');

INSERT INTO public.articles(
  id, name)
  VALUES
  (1, 'Twelve tips for engaging with biologists, as told by a physicist'),
  (2, 'A tunable orthogonal coiled-coil interaction toolbox for engineering mammalian cells'),
  (3, 'Structure and dynamics of the E. coli chemotaxis core signaling complex by cryo-electron tomography and molecular simulations');

INSERT INTO public.articles(
  id, name)
  VALUES
  (4, 'Optimizing High-Efficiency Quantum Memory with Quantum Machine Learning for Near-Term Quantum Devices'),
  (5, 'Anti-Zeno quantum advantage in fast-driven heat machines'),
  (6, 'Enhancing quantum annealing performance by a degenerate two-level system');

INSERT INTO public.articles(
  id, name)
  VALUES
  (7, 'Mathematical languages shape our understanding of time in physics'),
  (8, 'Is negative capacitance FET a steep-slope logic switch?'),
  (9, 'The multiscale physics of cilia and flagella'),
  (10, 'Whole-cortex mapping of common genetic influences on depression and a social deficits dimension'),
  (11, 'Guidelines for investigating causality of sequence variants in human disease');

INSERT INTO public.article_tag_references(
  id, article_id, tag_id)
  VALUES (1, 1, 3), (2, 2, 3), (3, 3, 3),
  (4, 4, 1), (5, 5, 1), (6, 6, 1),
  (7, 7, 2), (8, 8, 2), (9, 9, 2);
