SELECT a.name AS article, t.value AS tag FROM public.articles AS a
LEFT OUTER JOIN public.article_tag_references AS r ON a.id = r.article_id
LEFT OUTER JOIN public.tags AS t ON r.tag_id = t.id;
