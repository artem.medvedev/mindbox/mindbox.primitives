﻿using System;

using NUnit.Framework;

namespace Mindbox.Primitives.Tests
{
	[TestFixture]
	public class CircleTest
	{
		[TestCase(0)]
		[TestCase(-1)]
		public void CheckInvalidSides(double radius)
			=> Assert.That(() => new Circle(radius), Throws.TypeOf<ArgumentOutOfRangeException>());

		[TestCase(4, ExpectedResult = 8)]
		[TestCase(8, ExpectedResult = 16)]
		public double CheckDiameter(double radius) => new Circle(radius).Diameter;

		[TestCase(3, ExpectedResult = 18.85)]
		[TestCase(5, ExpectedResult = 31.42)]
		public double CheckPerimeter(double radius) => new Circle(radius).Perimeter;

		[TestCase(3, ExpectedResult = 18.85)]
		[TestCase(5, ExpectedResult = 31.42)]
		public double CheckCircumference(double radius) => new Circle(radius).Circumference;

		[TestCase(5, ExpectedResult = 78.54)]
		[TestCase(10, ExpectedResult = 314.16)]
		public double CheckArea(double radius) => new Circle(radius).Area;
	}
}
