﻿using System;
using System.Collections.Generic;

using NUnit.Framework;

namespace Mindbox.Primitives.Tests
{
	[TestFixture]
	public class ShapeTest
	{
		public static IEnumerable<TestCaseData> Shapes
			=> Array.AsReadOnly(new[]
			{
				new TestCaseData(new Circle(5)).Returns(78.54),
				new TestCaseData(new Rectangle(2, 2)).Returns(4),
				new TestCaseData(new Triangle(5, 5, 9)).Returns(9.81),
			});
		[TestCaseSource("Shapes")]
		public double CheckArea(Shape shapre) => shapre.Area;
	}
}
