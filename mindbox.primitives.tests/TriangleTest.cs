﻿using System;

using NUnit.Framework;

namespace Mindbox.Primitives.Tests
{
	[TestFixture]
	public class TriangleTest
	{
		[TestCase(5, 5, 11)]
		[TestCase(5, 11, 5)]
		[TestCase(11, 5, 5)]
		[TestCase(0, 1, 1)]
		[TestCase(1, 1, 0)]
		[TestCase(1, -1, 0)]
		public void CheckInvalidSides(double a, double b, double c)
			=> Assert.That(() => new Triangle(a, b, c), Throws.TypeOf<ArgumentOutOfRangeException>());

		[TestCase(5, 5, 9, ExpectedResult = false)]
		[TestCase(16, 63, 65, ExpectedResult = true)]
		[TestCase(16, 63, 65, ExpectedResult = true)]
		[TestCase(24, 143, 145, ExpectedResult = true)]
		public bool CheckTriangleIsRightAngled(double a, double b, double c) => new Triangle(a, b, c).IsRightAngled;

		[TestCase(2, 2, 3, ExpectedResult = 1.98)]
		[TestCase(5, 5, 9, ExpectedResult = 9.81)]
		public double CheckArea(double a, double b, double c) => new Triangle(a, b, c).Area;

		[TestCase(3, 3, 3, ExpectedResult = 9)]
		[TestCase(2, 2, 3, ExpectedResult = 7)]
		[TestCase(5, 5, 9, ExpectedResult = 19)]
		public double CheckPerimeter(double a, double b, double c) => new Triangle(a, b, c).Perimeter;
	}
}
