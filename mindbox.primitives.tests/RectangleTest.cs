﻿using System;

using NUnit.Framework;

namespace Mindbox.Primitives.Tests
{
	[TestFixture]
	public class RectangleTest
	{
		[TestCase(1, 0)]
		[TestCase(-1, 2)]
		public void CheckInvalidSides(double length, double width)
			=> Assert.That(() => new Rectangle(length, width), Throws.TypeOf<ArgumentOutOfRangeException>());

		[TestCase(2, 2, ExpectedResult = true)]
		[TestCase(5, 6, ExpectedResult = false)]
		public bool CheckRectangleIsSquare(double length, double width) => new Rectangle(length, width).IsSquare;

		[TestCase(2, 2, ExpectedResult = 4)]
		[TestCase(3, 3, ExpectedResult = 9)]
		public double CheckArea(double length, double width) => new Rectangle(length, width).Area;

		[TestCase(2, 2, ExpectedResult = 8)]
		[TestCase(3, 3, ExpectedResult = 12)]
		public double CheckPerimeter(double length, double width) => new Rectangle(length, width).Perimeter;
	}
}
