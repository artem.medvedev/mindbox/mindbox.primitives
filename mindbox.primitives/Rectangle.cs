﻿using System;

namespace Mindbox.Primitives
{
	public class Rectangle : Shape
	{
		public Rectangle(double length, double width)
		{
			if (width <= 0) throw new ArgumentOutOfRangeException(nameof(width), "Invalid triangle sides.");
			if (length <= 0) throw new ArgumentOutOfRangeException(nameof(length), "Invalid triangle sides.");

			Length = length;
			Width = width;
		}

		public double Length { get; }

		public double Width { get; }

		public override double Area => Length * Width;

		public override double Perimeter => 2 * Length + 2 * Width;

		public bool IsSquare => Length == Width;

		public double Diagonal => Math.Round(Math.Sqrt(Math.Pow(Length, 2) + Math.Pow(Width, 2)), 2);
	}
}
