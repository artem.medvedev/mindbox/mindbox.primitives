﻿using System;

namespace Mindbox.Primitives
{
	public class Triangle : Shape
	{
		private static readonly double E = 0.00001;

		public Triangle(double a, double b, double c)
		{
			if (a + b <= c || c <= 0) throw new ArgumentOutOfRangeException(nameof(c), "Invalid triangle sides.");
			if (a + c <= b || b <= 0) throw new ArgumentOutOfRangeException(nameof(b), "Invalid triangle sides.");
			if (b + c <= a || a <= 0) throw new ArgumentOutOfRangeException(nameof(a), "Invalid triangle sides.");

			A = a;
			B = b;
			C = c;
		}

		public double A { get; }

		public double B { get; }

		public double C { get; }

		private static bool Equals(double a, double b) => Math.Abs(a - b) < E;

		public bool IsRightAngled
		{
			get
			{
				if (A > B && A > C && Equals(A * A, B * B + C * C)) return true;
				if (B > C && B > C && Equals(B * B, C * C + A * A)) return true;
				if (C > A && C > B && Equals(C * C, A * A + B * B)) return true;
				return false;
			}
		}

		public override double Area
		{
			get
			{
				double hp = Perimeter / 2D;
				return Math.Round(Math.Sqrt(hp * (hp - A) * (hp - B) * (hp - C)), 2);
			}
		}

		public override double Perimeter => A + B + C;
	}
}
